module.exports = {
  defaultExtractor(content) {
    // From https://purgecss.com/guides/vue.html#usage
    const contentWithoutStyleBlocks = content.replace(
      /<style[^]+?<\/style>/gi,
      ""
    );
    return (
      contentWithoutStyleBlocks.match(/[A-Za-z0-9-_/:]*[A-Za-z0-9-_/]+!?/g) ||
      []
    );
  },
};
