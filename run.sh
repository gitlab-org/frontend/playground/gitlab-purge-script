#!/usr/bin/env bash

export DIR=$(dirname "$(readlink -f "$0")")
export GDK_DIR="$1"

if [ -z "$GDK_DIR" ]; then
	echo "ERROR: Missing path argument for GDK path. Please provide a path to the 'gitlab-development-kit/gitlab' project. Example:"
	echo ""
	echo "  ./run.sh ~/dev/projects/gitlab-development-kit/gitlab"
	echo ""
	exit 1
fi

bash ${DIR}/scripts/run_0*.sh
bash ${DIR}/scripts/run_1*.sh
bash ${DIR}/scripts/run_2*.sh
bash ${DIR}/scripts/run_3*.sh

