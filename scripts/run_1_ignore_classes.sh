echo ""
echo "STEP 1: Pull classes to ignore (from GitLab UI) ----------------"
echo ""

rm -rf ${DIR}/tmp/ignore
mkdir -p ${DIR}/tmp/ignore

cat ${GDK_DIR}/node_modules/@gitlab/ui/dist/index.css | ${DIR}/scripts/print_selectors.js > ${DIR}/tmp/ignore/gitlab_ui_index.json
cat ${GDK_DIR}/node_modules/@gitlab/ui/dist/utility_classes.css | ${DIR}/scripts/print_selectors.js > ${DIR}/tmp/ignore/gitlab_ui_utility_classes.json
