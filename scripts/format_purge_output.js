#!/usr/bin/env node
const stdFs = require("fs");
const fs = require("fs/promises");
const stdPath = require("path");

const IGNORE_DIR = stdPath.join(stdPath.dirname(__dirname), "tmp", "ignore");
const CLASS_REGEX = /\.[^#:.,\s]+/g;

async function readFilePathsFromDir(path) {
  try {
    const fileNames = await fs.readdir(path);

    return fileNames.map((x) => stdPath.join(path, x));
  } catch (e) {
    console.error("Could not read directory ", path);
    console.error(e);
    return [];
  }
}

async function readIgnoredClasses() {
  const filePaths = await readFilePathsFromDir(IGNORE_DIR);
  const classes = new Set();

  const allSelectors = await Promise.all(
    filePaths.map(async (x) => {
      const fileBuffer = await fs.readFile(x);
      return JSON.parse(fileBuffer.toString());
    })
  ).then((x) => x.flat());

  allSelectors.forEach((x) => {
    const matches = x.match(CLASS_REGEX) || [];

    matches.forEach((x) => classes.add(x));
  });

  return classes;
}

function groupByIgnored(selectors, ignoredClasses) {
  const ignored = [];
  const result = [];

  selectors.forEach((x) => {
    const selector = x.trim();
    const classesInSelector = selector.match(CLASS_REGEX);

    if (!classesInSelector) {
      result.push(selector);
    } else if (classesInSelector.some((x) => ignoredClasses.has(x))) {
      ignored.push(selector);
    } else {
      result.push(selector);
    }
  });

  return { ignored, result };
}

async function main() {
  const ignoredClasses = await readIgnoredClasses();
  const stdinBuffer = stdFs.readFileSync(0);
  const content = JSON.parse(stdinBuffer.toString());

  const ignoredSelectors = [];
  const newContent = content
    .filter((x) => {
      return x.rejected?.length;
    })
    .map((x) => {
      delete x["css"];
      const { ignored, result } = groupByIgnored(x.rejected, ignoredClasses);
      ignoredSelectors.push(...ignored);
      x.rejected = result;

      return x;
    });

  newContent.push({
    file: "Ignored (for example, from GitLab UI)",
    rejected: ignoredSelectors.sort(),
  });

  const newJson = JSON.stringify(newContent, undefined, 2);
  console.log(newJson);
}

main();
