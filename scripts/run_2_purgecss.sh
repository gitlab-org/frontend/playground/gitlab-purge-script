echo ""
echo "STEP 2: Run PurgeCSS --------------------"
echo ""

${DIR}/node_modules/.bin/purgecss --rejected  --config "${DIR}/purgecss.config.js" --css "${DIR}/tmp/assets/*.css" --content "${GDK_DIR}/{ee/,}app/**/*.{haml,rb}" --content "${GDK_DIR}/public/assets/webpack/**/*.js" > ${DIR}/tmp/output.json
cat ${DIR}/tmp/output.json | node ${DIR}/scripts/format_purge_output.js > ${DIR}/tmp/output_formatted.json
