#!/usr/bin/env node
const postcss = require("postcss");
const fs = require("fs");
const { brotliDecompress } = require("zlib");

function main() {
  const stdinBuffer = fs.readFileSync(0);
  const root = postcss.parse(stdinBuffer.toString());

  const selectors = new Set();
  root.walkRules((node) => {
    if (node.parent.type === "atrule" && node.parent.name === "keyframes") {
      return;
    }

    selectors.add(node.selector);
  });

  const resultJSON = JSON.stringify(
    Array.from(selectors.values()),
    undefined,
    2
  );
  console.log(resultJSON);
}

main();
