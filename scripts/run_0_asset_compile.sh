GDK_PUBLIC_DIR="${GDK_DIR}/public"
GDK_ASSETS_DIR="${GDK_PUBLIC_DIR}/assets"

echo ""
echo "STEP 0: Compile assets in GDK ----------------"
echo ""

if [ ! -d "${GDK_PUBLIC_DIR}" ]; then
	echo "ERROR: Could not find 'public/' directory in ${GDK_DIR}."
	echo "Are you sure you passed the correct path to your 'gitlab-development-kit/gitlab' project?"
	exit 1
fi

if [ -n "$(ls -A ${GDK_ASSETS_DIR})" ]; then
	echo "ERROR: Expected public/assets to not exist or be empty. Consider running:"
	echo ""
	echo "  rm -rf ${GDK_ASSETS_DIR}"
	echo ""
	exit 1
fi

cd $GDK_DIR

bundle && yarn
bundle exec rake gitlab:assets:compile
rm -rf ${DIR}/tmp/assets ${DIR}/tmp/output*
mkdir -p ${DIR}/tmp/assets
cp -r ${GDK_ASSETS_DIR}/**/*.css ${DIR}/tmp/assets

