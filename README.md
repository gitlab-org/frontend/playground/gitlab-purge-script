## gitlab-purge-script

This is a script that runs purgecss against GitLab to generate a report of unused selectors.

### How to run?

To run this against a local GDK:

```shell
./run.sh path/to/gitlab-development-kit/gitlab
```

